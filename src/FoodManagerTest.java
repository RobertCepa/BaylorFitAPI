import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.baylor.googlefit.FoodManager;
import edu.baylor.googlefit.GFitFoodManager;
import foodnetwork.serialization.FoodItem;
import foodnetwork.serialization.FoodNetworkException;
import foodnetwork.serialization.MealType;

/**
 * Test case for GFitFoodManager implementation of FoodManager interface.
 */
public class FoodManagerTest {
    static FoodManager mgr;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        mgr = new GFitFoodManager("975771840617", "src/testfitapi-ff60ac045afb.json");
    }

    /**
     * Tests addFood by asserting all its values.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    @Test
    public void testAddFood() throws FoodNetworkException {
        mgr.removeAllFoodItems();
        mgr.addFood("Plum", MealType.Breakfast, 3, "5.6");
        List<FoodItem> itemList = mgr.getFoodItems();
        assertEquals(itemList.get(0).getName(), "Plum");
        assertEquals(itemList.get(0).getMealType(), MealType.Breakfast);
        assertEquals(itemList.get(0).getCalories(), 3);
        assertEquals(itemList.get(0).getFat(), "5.6");
    }

    /**
     * Tests getFoodItems() by asserting its value to be not null.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    @Test
    public void testGetFoodItems() throws FoodNetworkException {
        List<FoodItem> itemList = mgr.getFoodItems();
        assertNotNull(itemList);
    }

    /**
     * Tests getFoodItems(long minutes) by adding one item and calling
     * getFoodItems(1). This item need to be there so we assert all its values.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    @Test
    public void testGetFoodItemsInLast2Minutes() throws FoodNetworkException {
        mgr.removeAllFoodItems();
        mgr.addFood("Plum", MealType.Breakfast, 3, "5.6");
        List<FoodItem> itemList = mgr.getFoodItems(1);
        assertEquals(itemList.get(0).getName(), "Plum");
        assertEquals(itemList.get(0).getMealType(), MealType.Breakfast);
        assertEquals(itemList.get(0).getCalories(), 3);
        assertEquals(itemList.get(0).getFat(), "5.6");
    }

    /**
     * Tests getLastModified() by asserting its return value not to be equal to
     * -1. It can never return -1 if we have at least one item in data source.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    @Test
    public void testGetLastModified() throws FoodNetworkException {
        mgr.addFood("Plum", MealType.Breakfast, 3, "5.6");
        long last = mgr.getLastModified();
        assertNotEquals(last, -1);
    }

    /**
     * Tests removeAllFoodItems() by asserting list of FoodItems to be empty
     * after removeAllFoodItems() call.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    @Test
    public void testRemoveAllFoodItems() throws FoodNetworkException {
        mgr.removeAllFoodItems();
        List<FoodItem> itemList = mgr.getFoodItems();
        assertTrue(itemList.isEmpty());
    }
}
