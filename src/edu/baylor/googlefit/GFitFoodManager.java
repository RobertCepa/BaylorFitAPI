package edu.baylor.googlefit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.fitness.Fitness;
import com.google.api.services.fitness.model.*;

import foodnetwork.serialization.FoodItem;
import foodnetwork.serialization.FoodNetworkException;
import foodnetwork.serialization.MealType;

import com.google.api.services.fitness.FitnessScopes;

/**
 * Manages addition of meals and reporting of food eaten
 */
public class GFitFoodManager implements FoodManager {
    /**
     * Name of this application
     */
    private final String APPLICATION_NAME = "BaylorFitAPI";

    /**
     * Scope of Google Fit API this application is working in
     */
    private final String DATA_TYPE_NAME = "com.google.nutrition";

    /**
     * The type of data source requests. When working with REST API, we need
     * raw.
     */
    private final String DATASOURCE_TYPE = "raw";

    /**
     * Name of the application data source
     */
    private final String DATASOURCE_NAME = "BaylorFitAPIDataStream";

    /**
     * Minimum start time we can use in i.e. Fit Sessions. It is equal to exact
     * number of nanoseconds since 1970
     */
    private final Long MIN_START_NS = 1397513334728708316L;

    /**
     * Maximum end time of session we can use. Is is equal to exact number of
     * nanoseconds since 1970. Note that those times do not matter for us in
     * this application, because we do not use sessions, but are required in
     * JSON requests.
     */
    private final Long MAX_END_NS = 1397515179728708316L;

    /**
     * Data set ID needs to be generated from minimum start time and maximum end
     * time (Google Fit API requirement)
     */
    private final String DATASET_ID = "0-2397515179728708316";

    /**
     * Name of the user connecting to API. Currently, only 'me' is allowed
     */
    private final String USER = "me";

    /**
     * Instance of Google Fit API service
     */
    private Fitness service;

    /**
     * Our single data set, shared across methods. This application only works
     * with one data set.
     */
    private Dataset instanceDataSet;

    /**
     * Milliseconds of last modified item. If the value is -1, we have no
     * informations or there are no FoodItems in storage.
     */
    private long lastModified = -1;

    /**
     * Path to a .json with credentials
     */
    private final String credentialsJsonPath;

    /**
     * ID of the application data source. Created from number of the project at
     * console.developers.google.com and from DATASOURCE_NAME.
     */
    private String dataSourceId = "raw:com.google.nutrition:";

    /**
     * Constructs a new Google Fit manager connected to Google's Fit API.
     * 
     * @param projectNumber
     *            Number of project accessible at console.developers.google.com
     *            in the IAM {@literal &} Admin settings section
     * @param credentialsJsonPath
     *            Path to local JSON with credentials
     * @throws FoodNetworkException
     *             if problem with initializing the Fit API
     */
    public GFitFoodManager(String projectNumber, String credentialsJsonPath) throws FoodNetworkException {
        this.credentialsJsonPath = credentialsJsonPath;
        this.dataSourceId += projectNumber;
        try {
            initialize();
        } catch (FileNotFoundException e) {
            throw new FoodNetworkException("Wrong path to JSON file.", e);
        } catch (IOException e) {
            throw new FoodNetworkException("Cannot create credentials.", e);
        } catch (GeneralSecurityException e) {
            throw new FoodNetworkException("Security problem.", e);
        }
    }

    /**
     * Initializes credentials, transport and own data source for this instance
     * of FoodManager. Every FoodManagerImpl instance has its own "space" of
     * data at Google. If you want to use that space in the future, i.e. in
     * another instances, you need to keep "instanceDataSet".
     * 
     * @throws FileNotFoundException
     *             if .json file with credentials is not found
     * @throws IOException
     *             if unable to create GoogleCredential from the given .json, or
     *             if unable to create GoogleNetHttpTransport
     * @throws GeneralSecurityException
     *             if unable to create GoogleNetHttpTransport
     * @throws FoodManagerException
     *             if unable to connect to a data source
     */
    private void initialize()
            throws FileNotFoundException, IOException, GeneralSecurityException, FoodNetworkException {
        DataTypeField nutrients = new DataTypeField().setName("nutrients").setFormat("map");
        DataTypeField meal_type = new DataTypeField().setName("meal_type").setFormat("integer").setOptional(true);
        DataTypeField food_item = new DataTypeField().setName("food_item").setFormat("string").setOptional(true);
        DataType dataType = new DataType().setName(DATA_TYPE_NAME)
                .setField(Arrays.asList(nutrients, meal_type, food_item));
        GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(credentialsJsonPath))
                .createScoped(Arrays.asList(FitnessScopes.FITNESS_NUTRITION_READ, FitnessScopes.FITNESS_NUTRITION_WRITE,
                        FitnessScopes.FITNESS_LOCATION_WRITE));
        HttpTransport http_transport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory json_factory = new JacksonFactory();
        service = new Fitness.Builder(http_transport, json_factory, credential).setApplicationName(APPLICATION_NAME)
                .build();
        Application app = new Application().setName(APPLICATION_NAME);
        DataSource content = new DataSource().setApplication(app).setType(DATASOURCE_TYPE).setName(DATASOURCE_NAME)
                .setDataType(dataType);
        // try to find the existing datasource before creating one
        Fitness.Users.DataSources.Get getRequest = service.users().dataSources().get(USER, dataSourceId);
        DataSource instanceDataSource;
        try {
            instanceDataSource = getRequest.execute();
        } catch (GoogleJsonResponseException e) {
            // if we dont have it, try to create one
            Fitness.Users.DataSources.Create createRequest = service.users().dataSources().create(USER, content);
            try {
                instanceDataSource = createRequest.execute();
            } catch (GoogleJsonResponseException ex) {
                throw new FoodNetworkException("Error with creating new datasource.", ex);
            }
        }
        if (instanceDataSource != null) {
            instanceDataSet = new Dataset().setDataSourceId(instanceDataSource.getDataStreamId())
                    .setMinStartTimeNs(MIN_START_NS).setMaxEndTimeNs(MAX_END_NS);
        }
    }

    @Override
    public void addFood(String name, MealType mealType, long calories, String fat) throws FoodNetworkException {
        Value nutrients = new Value().setMapVal(Arrays.asList(
                new ValueMapValEntry().setKey("fat.total").setValue(new MapValue().setFpVal(Double.parseDouble(fat))),
                new ValueMapValEntry().setKey("calories").setValue(new MapValue().setFpVal((double) calories))));
        // ordinal() values are 0,1,2,3 but we need 1,2,3,4 so we just increment
        // here
        Value type = new Value().setIntVal(mealType.ordinal() + 1);
        Value foodItem = new Value().setStringVal(name);
        DataPoint point = new DataPoint().setValue(Arrays.asList(nutrients, type, foodItem))
                .setStartTimeNanos(MIN_START_NS).setEndTimeNanos(MAX_END_NS).setDataTypeName(DATA_TYPE_NAME);
        instanceDataSet.setPoint(Arrays.asList(point));
        try {
            Fitness.Users.DataSources.Datasets.Patch request = service.users().dataSources().datasets().patch(USER,
                    instanceDataSet.getDataSourceId(), DATASET_ID, instanceDataSet);
            request.execute();
        } catch (IOException e) {
            throw new FoodNetworkException("Unable to add food item.", e);
        } finally {
            // if we add an item, we need to reset last modified information,
            // since we just sent new item to the server
            this.lastModified = -1;
        }
    }

    @Override
    public List<FoodItem> getFoodItems() throws FoodNetworkException {
        // if minutes ago is 0, we get all items stored (forever)
        return getFoodItems(0);
    }

    @Override
    public List<FoodItem> getFoodItems(long minutes) throws FoodNetworkException {
        List<FoodItem> foodItems = new ArrayList<>();
        try {
            Fitness.Users.DataSources.Datasets.Get request = service.users().dataSources().datasets().get(USER,
                    instanceDataSet.getDataSourceId(), DATASET_ID);
            List<DataPoint> dp = request.execute().getPoint();
            // if we have no points in dataset, Fit API returns null. In this
            // case, we return empty List.
            if (dp == null) {
                return foodItems;
            }
            // if we have specified minutes ago, we set millis to a real number,
            // otherwise set it to -1 to always meet condition in next loop
            long millis = minutes > 0 ? System.currentTimeMillis() - (minutes * 60000) : -1;
            FoodItem item = null;
            for (DataPoint element : dp) {
                if (element.getModifiedTimeMillis() >= millis) {
                    // transform Google's DataPoint to our custom FoodItem
                    // instance
                    try {
                        item = new FoodItem(element.getValue().get(2).getStringVal(),
                                MealType.getMealType(toEnumCharTypes(element.getValue().get(1).getIntVal().intValue())),
                                (long) element.getValue().get(0).getMapVal().get(1).getValue().getFpVal().doubleValue(),
                                Double.toString(element.getValue().get(0).getMapVal().get(0).getValue().getFpVal()));
                        foodItems.add(item);
                    } catch (FoodNetworkException ex) {
                    }
                }
                if (element.getModifiedTimeMillis() > this.lastModified) {
                    this.lastModified = element.getModifiedTimeMillis();
                }
            }
            return foodItems;
        } catch (IOException e) {
            throw new FoodNetworkException("Unable to get food items.", e);
        }
    }

    @Override
    public long getLastModified() throws FoodNetworkException {
        if (this.lastModified == -1) {
            getFoodItems(0);
        }
        return this.lastModified;
    }

    /**
     * Converts int codes to char codes
     * 
     * @param code
     *            Code (int) of meal type
     * @return Code (char) of meal type
     */
    private char toEnumCharTypes(final int code) {
        // Google can return 0 occasionally if meal type is not specified, in
        // this case lets return SNACK
        switch (code) {
        case 1:
            return 'B';
        case 2:
            return 'L';
        case 3:
            return 'D';
        case 4:
            return 'S';
        default:
            return 'S';
        }
    }

    @Override
    public void removeAllFoodItems() throws FoodNetworkException {
        try {
            Fitness.Users.DataSources.Datasets.Delete req = service.users().dataSources().datasets().delete("me", instanceDataSet.getDataSourceId(), DATASET_ID);
            req.execute();
        } catch (IOException e) {
            throw new FoodNetworkException("Unable to remove food items", e);
        }
    }
}
