package edu.baylor.googlefit;

import java.util.List;

import foodnetwork.serialization.FoodItem;
import foodnetwork.serialization.FoodNetworkException;
import foodnetwork.serialization.MealType;

/**
 * Manages addition of meals and reporting of food eaten
 */

public interface FoodManager {
    /**
     * Add a food item
     * 
     * @param name
     *            Name of food item
     * @param mealType
     *            Type of meal (breakfast, lunch, dinner, snack)
     * @param calories
     *            Number (long) of calories
     * @param fat
     *            Number (string) of grams of fat
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    void addFood(String name, MealType mealType, long calories, String fat) throws FoodNetworkException;

    /**
     * Get list of added food items
     * 
     * @return List of all food items (FoodItem) eaten
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    List<FoodItem> getFoodItems() throws FoodNetworkException;

    /**
     * Get list of food items added in the last given minutes
     * 
     * @param minutes
     *            Specifies how old data (in minutes) we want to get
     * @return List of all food items (FoodItem) eaten in the time interval
     *         {@literal <}now - minutes, now{@literal >}
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    List<FoodItem> getFoodItems(long minutes) throws FoodNetworkException;

    /**
     * Get timestamp of last update of master food item list
     * 
     * @return last modified timestamp
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    long getLastModified() throws FoodNetworkException;

    /**
     * Removes all food items.
     * 
     * @throws FoodNetworkException
     *             if communication or server problem
     */
    void removeAllFoodItems() throws FoodNetworkException;
}