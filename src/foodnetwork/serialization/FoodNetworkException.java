package foodnetwork.serialization;

/**
 * Exception for food network handling
 */
public class FoodNetworkException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs food network exception
     * 
     * @param message
     *            exception message
     * @param cause
     *            exception cause
     */
    public FoodNetworkException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs food network exception with null cause
     * 
     * @param message
     *            exception message
     */
    public FoodNetworkException(final String message) {
        super(message);
    }
}
