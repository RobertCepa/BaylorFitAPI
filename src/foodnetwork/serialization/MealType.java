package foodnetwork.serialization;

import foodnetwork.serialization.FoodNetworkException;
import foodnetwork.serialization.MealType;

/**
 * Meal type
 */
public enum MealType {
    /**
     * Breakfast
     */
    Breakfast('B'),
    /**
     * Lunch
     */
    Lunch('L'),
    /**
     * Dinner
     */
    Dinner('D'),
    /**
     * Snack
     */
    Snack('S');
    private char mealTypeCode;

    private MealType(final char mealTypeCode) {
        this.mealTypeCode = mealTypeCode;
    }

    /**
     * Get code for meal type
     * 
     * @return meal type code
     */
    public char getMealTypeCode() {
        return mealTypeCode;
    }

    /**
     * Get meal type for given code
     * 
     * @param code
     *            code of meal type
     * 
     * @return meal type corresponding to code
     * 
     * @throws FoodNetworkException
     *             if bad code value
     */
    public static MealType getMealType(final char code) throws FoodNetworkException {
        for (MealType t : values()) {
            if (t.getMealTypeCode() == code) {
                return t;
            }
        }
        throw new FoodNetworkException("Illegal code: " + code);
    }
};