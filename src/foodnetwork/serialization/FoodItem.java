package foodnetwork.serialization;

/**
 * Represents FoodItem and provides serialization/deserialization
 */
public class FoodItem {
  private String name;
  private MealType mealType;
  private long calories;
  private String fat;
  
  /**
   * Constructs food item with set values
   * 
   * @param name name of food item
   * @param mealType type of meal
   * @param calories number of calories in food item
   * @param fat grams of fat in food item
   * 
   * @throws FoodNetworkException if validation fails
   */
  public FoodItem(final String name,
      final MealType mealType, final long calories, final String fat)
          throws FoodNetworkException {
    setName(name);
    setMealType(mealType);
    setCalories(calories);
    setFat(fat);
  }

  /**
   * Returns human-readable string
   */
  @Override
  public String toString() {
    return "Name " + name + ":" + calories + " - " + fat;
  }

  /**
   * Returns name
   * 
   * @return name
   */
  public final String getName() {
    return name;
  }

  /**
   * Sets name
   * 
   * @param name new name
   * 
   * @throws FoodNetworkException if validation fails
   */
  public final void setName(final String name) throws FoodNetworkException {
    this.name = name;
  }

  /**
   * Returns meal type
   * 
   * @return meal type
   */
  public final MealType getMealType() {
    return mealType;
  }

  /**
   * Sets meal type
   * 
   * @param mealType new mealType
   * 
   * @throws FoodNetworkException if validation fails
   */
  public final void setMealType(final MealType mealType) throws FoodNetworkException {
    this.mealType = mealType;
  }

  /**
   * Returns calories
   * 
   * @return calories
   */
  public final long getCalories() {
    return calories;
  }

  /**
   * Sets calories
   * 
   * @param calories new calories
   * 
   * @throws FoodNetworkException if validation fails
   */
  public final void setCalories(final long calories) throws FoodNetworkException {
    this.calories = calories;
  }

  /**
   * Returns grams of fat
   * 
   * @return grams of fat
   */
  public final String getFat() {
    return fat;
  }

  /**
   * Sets fat
   * 
   * @param fat new fat grams
   * 
   * @throws FoodNetworkException if validation fails
   */
  public final void setFat(final String fat) throws FoodNetworkException {
    this.fat = fat;
  }
}
